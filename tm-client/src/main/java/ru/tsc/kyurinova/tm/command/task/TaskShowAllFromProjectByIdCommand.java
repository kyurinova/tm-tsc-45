package ru.tsc.kyurinova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractTaskCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kyurinova.tm.endpoint.ProjectDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskShowAllFromProjectByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-show-by-project-id";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Show all tasks by project id...";
    }

    @Override
    public void execute() {
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        @Nullable final ProjectDTO project = serviceLocator.getProjectEndpoint().findByIdProject(session, projectId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        System.out.println(serviceLocator.getProjectTaskEndpoint().findAllTaskByProjectId(session, projectId));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
