package ru.tsc.kyurinova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;
import ru.tsc.kyurinova.tm.enumerated.Role;
import ru.tsc.kyurinova.tm.endpoint.UserDTO;
import ru.tsc.kyurinova.tm.util.TerminalUtil;

public class UserViewProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "view-user";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "view user profile...";
    }

    @Override
    public void execute() {
        @NotNull final SessionDTO session = serviceLocator.getSessionService().getSession();
        System.out.println("Enter login:");
        @NotNull final String login = TerminalUtil.nextLine();
        @Nullable final UserDTO user = serviceLocator.getUserEndpoint().findByLoginUser(session, login);

        System.out.println("[VIEW PROFILE]");
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}
