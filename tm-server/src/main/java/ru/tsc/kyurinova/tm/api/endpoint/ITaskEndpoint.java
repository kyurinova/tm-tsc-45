package ru.tsc.kyurinova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface ITaskEndpoint {

    @WebMethod
    void removeTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "entity", partName = "entity")
                    TaskDTO entity
    );

    @WebMethod
    @NotNull
    List<TaskDTO> findAllTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @WebMethod
    void clearTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @WebMethod
    @Nullable
    TaskDTO findByIdTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull
    TaskDTO findByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexTaskUserId(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @NotNull
            @WebParam(name = "index", partName = "index")
                    Integer index
    );


    @WebMethod
    void removeTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @WebParam(name = "entity", partName = "entity")
                    TaskDTO entity
    );

    @WebMethod
    @NotNull
    List<TaskDTO> findAllTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTaskSorted(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "sort", partName = "sort")
                    String sort
    );

    @WebMethod
    void clearTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    );

    @WebMethod
    @Nullable
    TaskDTO findByIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    @NotNull
    TaskDTO findByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void removeByIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void removeByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    boolean existsByIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    boolean existsByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void createTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void createTaskDescr(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    @NotNull
    TaskDTO findByNameTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void removeByNameTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void updateByIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    void updateByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @NotNull
            @WebParam(name = "description", partName = "description")
                    String description
    );

    @WebMethod
    void startByIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void startByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void startByNameTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void finishByIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    );

    @WebMethod
    void finishByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    );

    @WebMethod
    void finishByNameTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name
    );

    @WebMethod
    void changeStatusByIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    void changeStatusByIndexTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    void changeStatusByNameTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "name", partName = "name")
                    String name,
            @Nullable
            @WebParam(name = "status", partName = "status")
                    Status status
    );

    @WebMethod
    @Nullable
    TaskDTO findByProjectAndTaskIdTask(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "projectId", partName = "projectId")
                    String projectId,
            @Nullable
            @WebParam(name = "taskId", partName = "taskId")
                    String taskId
    );
}
