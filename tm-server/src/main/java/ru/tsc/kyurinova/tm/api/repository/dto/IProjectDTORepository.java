package ru.tsc.kyurinova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectDTORepository {

    void add(
            @NotNull final String userId,
            @NotNull final ProjectDTO project
    );

    @Nullable
    ProjectDTO findByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void startById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void startByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void startByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void finishById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void finishByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void update(
            @NotNull final String userId,
            @NotNull final ProjectDTO project
    );

    void changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    );

    void changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    );

    void changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    );

    void remove(
            @NotNull final String userId,
            @NotNull final ProjectDTO project
    );

    @NotNull
    List<ProjectDTO> findAll(
    );

    @NotNull
    List<ProjectDTO> findAllUserId(
            @NotNull final String userId
    );

    void clear(
    );

    void clearUserId(
            @NotNull final String userId
    );

    @Nullable
    ProjectDTO findById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Nullable
    ProjectDTO findByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

}