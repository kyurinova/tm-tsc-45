package ru.tsc.kyurinova.tm.exception.entity;

import ru.tsc.kyurinova.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error. Project not found.");
    }

}
