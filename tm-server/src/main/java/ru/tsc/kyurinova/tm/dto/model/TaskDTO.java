package ru.tsc.kyurinova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.entity.IWBS;
import ru.tsc.kyurinova.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class TaskDTO extends AbstractOwnerEntityDTO implements IWBS {

    public TaskDTO(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public TaskDTO(@NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    public TaskDTO(@NotNull String userId, @NotNull String name, @NotNull String description) {
        this.userId = userId;
        this.name = name;
        this.description = description;
    }

    public TaskDTO(@NotNull String userId, @NotNull String name, @NotNull String description, @Nullable Date startDate) {
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
    }

    @NotNull
    @Column
    private String name = "";

    @NotNull
    @Column(name = "descr")
    private String description = "";

    @NotNull
    @Column
    private Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "project_id")
    private String projectId = null;

    @Nullable
    @Column(name = "start_dt")
    private Date startDate;

    @Nullable
    @Column(name = "finish_dt")
    private Date finishDate;

    @NotNull
    @Column
    private Date created = new Date();

}
