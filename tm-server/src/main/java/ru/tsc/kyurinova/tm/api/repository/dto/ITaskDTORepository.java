package ru.tsc.kyurinova.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.enumerated.Status;

import ru.tsc.kyurinova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskDTORepository {

    void add(
            @NotNull final String userId,
            @NotNull final TaskDTO task
    );

    @Nullable
    TaskDTO findByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void removeByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void startById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void startByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void startByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void finishById(
            @NotNull final String userId,
            @NotNull final String id
    );

    void finishByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void finishByName(
            @NotNull final String userId,
            @NotNull final String name
    );

    void update(
            @NotNull final String userId,
            @NotNull final TaskDTO task
    );

    void changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    );

    void changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    );

    void changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    );

    void bindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    );

    void unbindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    );

    @Nullable
    TaskDTO findByProjectAndTaskId(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    );

    void removeAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    @NotNull
    List<TaskDTO> findAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    );

    void remove(
            @NotNull final String userId,
            @NotNull final TaskDTO task
    );

    @NotNull
    List<TaskDTO> findAll(
    );

    @NotNull
    List<TaskDTO> findAllUserId(
            @NotNull final String userId
    );

    void clearUserId(
            @NotNull final String userId
    );

    void clear(
    );

    @Nullable
    TaskDTO findById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Nullable
    TaskDTO findByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    );

    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

}
