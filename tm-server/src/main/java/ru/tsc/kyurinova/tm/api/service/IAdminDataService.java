package ru.tsc.kyurinova.tm.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.dto.Domain;

public interface IAdminDataService {

    @SneakyThrows
    @NotNull
    Domain getDomain();

    @SneakyThrows
    void setDomain(@Nullable Domain domain);

    @SneakyThrows
    void dataBackupLoad();

    @SneakyThrows
    void dataBackupSave();

    @SneakyThrows
    void dataBase64Load();

    @SneakyThrows
    void dataBase64Save();

    @SneakyThrows
    void dataBinaryLoad();

    @SneakyThrows
    void dataBinarySave();

    @SneakyThrows
    void dataJsonLoadFasterXML();

    @SneakyThrows
    void dataJsonLoadJaxB();

    @SneakyThrows
    void dataJsonSaveFasterXML();

    @SneakyThrows
    void dataJsonSaveJaxB();

    @SneakyThrows
    void dataXmlLoadFasterXML();

    @SneakyThrows
    void dataXmlLoadJaxBC();

    @SneakyThrows
    void dataXmlSaveFasterXML();

    @SneakyThrows
    void dataXmlSaveJaxB();

    @SneakyThrows
    void dataYamlLoadFasterXML();

    @SneakyThrows
    void dataYamlSaveFasterXML();

}
