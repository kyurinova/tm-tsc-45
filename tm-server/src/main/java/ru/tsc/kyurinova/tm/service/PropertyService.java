package ru.tsc.kyurinova.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "config.properties";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_NAME = "developer.name";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT = "";

    @NotNull
    private static final String DEVELOPER_EMAIL = "developer.email";

    @NotNull
    private static final String SESSION_SECRET_KEY = "session.secret";

    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";

    @NotNull
    private static final String SESSION_ITERATION_KEY = "session.iteration";

    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String SERVER_HOST = "server.host";

    @NotNull
    private static final String DEFAULT_SERVER_HOST = "localhost";

    @NotNull
    private static final String SERVER_PORT = "server.port";

    @NotNull
    private static final String DEFAULT_SERVER_PORT = "8081";

    @NotNull
    private static final String JDBC_USER_KEY = "jdbc.user";

    @NotNull
    private static final String JDBC_USER_DEFAULT_VALUE = "postgres";

    @NotNull
    private static final String JDBC_USER_PASSWORD_KEY = "jdbc.password";

    @NotNull
    private static final String JDBC_USER_PASSWORD_DEFAULT_VALUE = "postgres";

    @NotNull
    private final static String JDBC_URL_KEY = "jdbc.url";

    @NotNull
    private final static String JDBC_URL_DEFAULT_VALUE = "jdbc:postgresql://localhost/task-manager";

    @NotNull
    private final static String JDBC_DRIVER_KEY = "jdbc.driver";

    @NotNull
    private final static String JDBC_DRIVER_DEFAULT_VALUE = "org.postgresql.Driver";

    @NotNull
    private final static String JDBC_SQL_DIALECT_KEY = "jdbc.sql_dialect";

    @NotNull
    private final static String JDBC_SQL_DIALECT_VALUE = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private final static String JDBC_HBM2DDL_AUTO_KEY = "jdbc.hbm2ddl_auto";

    @NotNull
    private final static String JDBC_HBM2DDL_AUTO_VALUE = "update";

    @NotNull
    private final static String JDBC_SHOW_SQL_KEY = "jdbc.show_sql";

    @NotNull
    private final static String JDBC_SHOW_SQL_VALUE = "true";

    @NotNull
    private static final String DB_FORMAT_SQL_KEY = "database.format_sql";

    @NotNull
    private static final String DB_FORMAT_SQL_EMPTY_VALUE = "true";

    @NotNull
    private static final String DB_SECOND_LEVEL_CACHE_KEY = "database.second_lvl_cache";

    @NotNull
    private static final String DB_SECOND_LEVEL_CACHE_EMPTY_VALUE = "true";

    @NotNull
    private static final String DB_CACHE_REGION_FACTORY_KEY = "database.factory_class";

    @NotNull
    private static final String DB_CACHE_REGION_FACTORY_EMPTY_VALUE = "com.hazelcast.hibernate.HazelcastCacheRegionFactory";

    @NotNull
    private static final String DB_USE_QUERY_CACHE_KEY = "database.use_query_cache";

    @NotNull
    private static final String DB_USE_QUERY_CACHE_EMPTY_VALUE = "true";

    @NotNull
    private static final String DB_USE_MIN_PUTS_KEY = "database.use_min_puts";

    @NotNull
    private static final String DB_USE_MIN_PUTS_EMPTY_VALUE = "true";

    @NotNull
    private static final String DB_CACHE_REGION_PREFIX_KEY = "database.region_prefix";

    @NotNull
    private static final String DB_CACHE_REGION_PREFIX_EMPTY_VALUE = "task-manager";

    @NotNull
    private static final String DB_CACHE_PROVIDER_CONFIG_KEY = "database.config_file_path";

    @NotNull
    private static final String DB_CACHE_PROVIDER_CONFIG_EMPTY_VALUE = "hazelcast.xml";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    public String getValue(final String value, final String defaultValue) {
        @Nullable final String systemProperty = System.getProperty(value);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(value);
        if (environmentProperty != null) return environmentProperty;

        return properties.getProperty(value, defaultValue);
    }

    @NotNull
    public Integer getValueInt(final String value, final String defaultValue) {
        @Nullable final String systemProperty = System.getProperty(value);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(value);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);

        return Integer.parseInt(
                properties.getProperty(value, defaultValue)
        );
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getValueInt(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getValue(DEVELOPER_NAME, DEVELOPER_NAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getValue(DEVELOPER_EMAIL, DEVELOPER_EMAIL_DEFAULT);
    }

    @Override
    @NotNull
    public String getSessionSecret() {
        return getValue(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

    @Override
    @NotNull
    public Integer getSessionIteration() {
        return getValueInt(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getValue(SERVER_HOST, DEFAULT_SERVER_HOST);
    }

    @Override
    @NotNull
    public String getServerPort() {
        return getValue(SERVER_PORT, DEFAULT_SERVER_PORT);
    }

    @NotNull
    @Override
    public String getJdbcUser() {
        return getValue(JDBC_USER_KEY, JDBC_USER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return getValue(JDBC_USER_PASSWORD_KEY, JDBC_USER_PASSWORD_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return getValue(JDBC_URL_KEY, JDBC_URL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return getValue(JDBC_DRIVER_KEY, JDBC_DRIVER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcSqlDialect() {
        return getValue(JDBC_SQL_DIALECT_KEY, JDBC_SQL_DIALECT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcNbm2ddlAuto() {
        return getValue(JDBC_HBM2DDL_AUTO_KEY, JDBC_HBM2DDL_AUTO_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcShowSql() {
        return getValue(JDBC_SHOW_SQL_KEY, JDBC_SHOW_SQL_VALUE);
    }

    @NotNull
    @Override
    public String getFormatSql() {
        return getValue(DB_FORMAT_SQL_KEY, DB_FORMAT_SQL_EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getSecondLevelCache() {
        return getValue(DB_SECOND_LEVEL_CACHE_KEY, DB_SECOND_LEVEL_CACHE_EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getCacheRegionFactory() {
        return getValue(DB_CACHE_REGION_FACTORY_KEY, DB_CACHE_REGION_FACTORY_EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getUseQueryCache() {
        return getValue(DB_USE_QUERY_CACHE_KEY, DB_USE_QUERY_CACHE_EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getUseMinPuts() {
        return getValue(DB_USE_MIN_PUTS_KEY, DB_USE_MIN_PUTS_EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getCacheRegionPrefix() {
        return getValue(DB_CACHE_REGION_PREFIX_KEY, DB_CACHE_REGION_PREFIX_EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getCacheProviderConfig() {
        return getValue(DB_CACHE_PROVIDER_CONFIG_KEY, DB_CACHE_PROVIDER_CONFIG_EMPTY_VALUE);
    }

}
