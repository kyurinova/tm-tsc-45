package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.repository.dto.UserDTORepository;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;

public class UserRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final UserDTO user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.ru";

    public EntityManager GetEntityManager() {
        return connectionService.getEntityManager();
    }

    public UserRepositoryTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        user = new UserDTO();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        user.setPasswordHash(HashUtil.salt("testUser", 3, "test"));
    }

    @Before
    public void before() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findByUserTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        Assert.assertEquals(user.getId(), userRepository.findById(userId).getId());
        Assert.assertEquals(user.getId(), userRepository.findByIndex(0).getId());
        Assert.assertEquals(user.getId(), userRepository.findByLogin(userLogin).getId());
        Assert.assertEquals(user.getId(), userRepository.findByEmail(userEmail).getId());
    }

    @Test
    public void removeUserTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        Assert.assertNotNull(user);
        entityManager.getTransaction().begin();
        userRepository.remove(user);
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        Assert.assertNotNull(user);
        Assert.assertNotNull(userLogin);
        entityManager.getTransaction().begin();
        userRepository.removeByLogin(userLogin);
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        Assert.assertNotNull(user);
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        userRepository.removeById(userId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByIdIndex() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        Assert.assertNotNull(user);
        entityManager.getTransaction().begin();
        userRepository.removeByIndex(0);
        entityManager.getTransaction().commit();
        Assert.assertNull(userRepository.findByIndex(0));
    }

    @After
    public void after() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
