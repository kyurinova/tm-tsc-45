package ru.tsc.kyurinova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kyurinova.tm.api.repository.dto.IProjectDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.ITaskDTORepository;
import ru.tsc.kyurinova.tm.api.repository.dto.IUserDTORepository;
import ru.tsc.kyurinova.tm.api.service.IConnectionService;
import ru.tsc.kyurinova.tm.api.service.IPropertyService;
import ru.tsc.kyurinova.tm.enumerated.Status;
import ru.tsc.kyurinova.tm.dto.model.ProjectDTO;
import ru.tsc.kyurinova.tm.dto.model.TaskDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;
import ru.tsc.kyurinova.tm.repository.dto.ProjectDTORepository;
import ru.tsc.kyurinova.tm.repository.dto.TaskDTORepository;
import ru.tsc.kyurinova.tm.repository.dto.UserDTORepository;
import ru.tsc.kyurinova.tm.service.ConnectionService;
import ru.tsc.kyurinova.tm.service.PropertyService;
import ru.tsc.kyurinova.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.sql.Timestamp;

public class TaskRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private TaskDTO task;

    @NotNull
    private String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private ProjectDTO project;

    @NotNull
    private String projectId;

    @NotNull
    private final static String PROJECT_NAME = "testProject";

    @NotNull
    private final String userId;

    public EntityManager GetEntityManager() {
        return connectionService.getEntityManager();
    }

    public TaskRepositoryTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull final UserDTO user = new UserDTO();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 3, "test"));
        userRepository.add(user);
        entityManager.getTransaction().commit();
    }

    @Before
    public void before() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        task = new TaskDTO();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setCreated(new Timestamp(task.getCreated().getTime()));
        taskRepository.add(userId, task);
        project = new ProjectDTO();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(PROJECT_NAME);
        project.setCreated(new Timestamp(project.getCreated().getTime()));
        projectRepository.add(userId, project);
        entityManager.getTransaction().commit();
    }

    @Test
    public void findTaskTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task.getId(), taskRepository.findById(userId, taskId).getId());
        Assert.assertEquals(task.getId(), taskRepository.findByIndex(userId, 0).getId());
        Assert.assertEquals(task.getId(), taskRepository.findByName(userId, taskName).getId());
    }

    @Test
    public void existsTaskTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertEquals(task.getId(), taskRepository.findById(userId, taskId).getId());
    }

    @Test
    public void removeTaskByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.removeById(userId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAllUserId(userId).isEmpty());
    }

    @Test
    public void removeTaskByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.removeByName(userId, taskName);
        entityManager.getTransaction().commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void startByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.startById(userId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void startByIndexTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.startByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.startByName(userId, taskName);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void finishByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.finishById(userId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void finishByIndexTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.finishByIndex(userId, 0);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.finishByName(userId, taskName);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusById(userId, taskId, Status.COMPLETED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusById(userId, taskId, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByName(userId, taskName, Status.IN_PROGRESS);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByName(userId, taskName, Status.COMPLETED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
        entityManager.getTransaction().begin();
        taskRepository.changeStatusByName(userId, taskName, Status.NOT_STARTED);
        entityManager.getTransaction().commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void findByProjectAndTaskIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task.getId(), taskRepository.findByProjectAndTaskId(userId, projectId, taskId).getId());
    }

    @Test
    public void findAllTaskByProjectIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(task.getId(), taskRepository.findAllTaskByProjectId(userId, projectId).get(0).getId());
    }

    @Test
    public void bindTaskToProjectByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void unbindTaskByIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        entityManager.getTransaction().begin();
        taskRepository.unbindTaskToProjectById(userId, projectId, taskId);
        entityManager.getTransaction().commit();
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void removeAllTaskByProjectIdTest() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final TaskDTO task1 = new TaskDTO();
        task1.setUserId(userId);
        entityManager.getTransaction().begin();
        taskRepository.add(userId, task1);
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        entityManager.getTransaction().commit();
        Assert.assertEquals(2, taskRepository.findAllTaskByProjectId(userId, projectId).size());
        entityManager.getTransaction().begin();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        entityManager.getTransaction().commit();
        Assert.assertEquals(0, taskRepository.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void after() {
        @NotNull final EntityManager entityManager = GetEntityManager();
        @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
        @NotNull final IUserDTORepository userRepository = new UserDTORepository(entityManager);
        @NotNull final IProjectDTORepository projectRepository = new ProjectDTORepository(entityManager);
        entityManager.getTransaction().begin();
        taskRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        projectRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.getTransaction().begin();
        userRepository.clear();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}
